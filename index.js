const fork = require('child_process').fork;
const Response = require('./response');

class Capture{
	constructor(port, prefix){
		process.on('exit', ()=>{
			this.mockServer.kill()
		})
		this.prefix = prefix
		this.postCallbacks = {}
		this.getCallbacks = {}
		this.mockServer = fork(__dirname + '/server.js', [port])
		this.mockServer.on('message', request =>{
			if (request.status == 1){
				return this.onListenCallback ? this.onListenCallback() : false;
			}
			if (request.method == 'GET' && this.getCallbacks[request.url]){
				this.getCallbacks[request.url](request, new Response(this.mockServer))
			}
			if (request.method == 'POST' && this.postCallbacks[request.url]){
				this.postCallbacks[request.url](request, new Response(this.mockServer))
			}
			if (request.method == 'OPTIONS'){
				this.mockServer.send({statusCode:200,body:'OK'})
			}
		})
		this.mockServer.on('exit', ()=>{
			if (this.killCallback) this.killCallback()
		})
	}
	onListen(callback){
		if (callback) this.onListenCallback = callback
	}
	onPost(url, callback){
		let prefix = this.prefix
		if (url.indexOf('/') !== 0) url = '/'+url
		if (prefix.indexOf('/') !== 0 && prefix) prefix = '/'+prefix
		this.postCallbacks[prefix+url] = callback
	}
  onGet(url, callback){
    let prefix = this.prefix
		if (url.indexOf('/') !== 0) url = '/'+url
		if (prefix.indexOf('/') !== 0 && prefix) prefix = '/'+prefix
		this.getCallbacks[prefix+url] = callback
  }
	kill(callback){
		this.killCallback = callback
		this.mockServer.send('END')
		setTimeout(()=>{
			this.mockServer.kill()
		},100)
	}
  removeAllListeners(){
    this.postCallbacks = {}
  }
}

module.exports = Capture

