const http = require('http');
let res = null
let req = null
let endOnKill = false
const server = http.createServer((newReq, newRes) => {
	let body = ''
	newReq.on('data',chunk => {
		body += chunk;
	}).on('end', ()=>{

		process.send({
			body,
			method:newReq.method,
			url:newReq.url,
		})
	})
	res = newRes;
	req = newReq

});

process.on('message', message => {
	if (!res) return
	if (message == 'END') return res.end()
	res.setHeader('Access-Control-Allow-Origin', req.headers.origin || '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
	res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
	res.setHeader('Access-Control-Allow-Credentials', 'true');
	res.writeHead(message.statusCode)
	if (message.body) res.write(message.body)
	res.end()
})

server.listen(process.argv[2], ()=>{
	process.send({status:1})
});