const assert = require('assert')
const Capture = require('./');
const net = require('net');
const request = require('request');

describe('node-capture mocking server', function(){
	let port = 3100
	let api = '/api'
	
	it(`should create a server on port ${port}, with prefix ${api}`, function(done){
		let capture = new Capture(port, api)
	
		const callback = function(status){
			capture.kill(function(){
				done(status)
			})
		}
		const server = net.createServer(function(socket){
			socket.write('Echo server\r\n');
		})
		server.on('error', function(err){
		  callback()
		});
	
		capture.onListen(function(){
			server.listen(port, function() {
				callback(`I should not be able to create another listener on ${port} here`)
			});
		})
	})
	
	it(`should create api /test with response code 200 and body {status:true}`, function(done){
		let capture = new Capture(port, 'api')
		capture.onPost('test', function(req, res){
		  res.statusCode = 200
			res.json({status:true})
		})
	
		capture.onListen(function(){
			request.post(`http://localhost:${port}/api/test`,{hello:'world'}, function(err, res, body){
				capture.kill(done)
				assert(res.statusCode == 200 && JSON.parse(body).status === true)
			})
		})
	})
	
	it(`should capture post data`, function(done){
		const myGreatMethod = function(){
		  request({
				method: 'POST', 
				uri:`http://localhost:${port}/api/test`, 
				body:{hello:'world'}, 
				json:true
			})
		}
	
		const capture = new Capture(port, 'api')
		capture.onPost('test', function(req, res) {
			capture.kill(done)
			assert(JSON.parse(req.body).hello == 'world') 
		})
	
		capture.onListen(function(){ 
		  myGreatMethod()
		})
	})
	
  it(`should mock a json response on POST /create request`, function(done){
    const capture = new Capture(port, 'api')
    capture.onPost('/create', function(req, res){
      res.json({status:true})
    })
	
		capture.onListen(function(){ 
			request({
	      method:'POST', 
	      uri:`http://localhost:${port}/api/create`, 
				body:'', 
	    }, function(err,res,body){
				capture.kill(done)
				assert(JSON.parse(body).status===true)
	    })
		})
  })
	
  it(`should accept api endpoint as ''`, function(done){
    const capture = new Capture(port, '')
    capture.onPost('/create', function(req, res){
      capture.kill(done)
    })
	
    capture.onListen(function(){ 
      request({
        method:'POST', 
        uri:`http://localhost:${port}/create`, 
        body:'', 
      })
    })
  })
	
  it(`should accept GET HTTP request`, function(done){
    const capture = new Capture(port, '')
    capture.onGet('/test', function(req, res){
      capture.kill(done)
    })
	
    capture.onListen(function(){ 
      request({
        method:'GET', 
        uri:`http://localhost:${port}/test`
      })
    })
  })
	
})