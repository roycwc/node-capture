class Response {
	constructor(ipc){
		this.statusCode = 200
		this.ipc = ipc
	}
	json(obj){
		this.send(JSON.stringify(obj))
	}
	send(str){
		this.ipc.send({
			statusCode:this.statusCode,
			body:str
		})
	}
  end(){
    this.send('')
  }
}

module.exports = Response 
